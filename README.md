# MongoDB Studio 3T 无限试用
## 0.有效版本
1. 2019.x.x
2. 最新2020.3.1
## 1.相关
* javaagent
* javassist
## 2. 使用
1. maven打包`mvn package`
2. jar包放置到`Studio 3T`安装目录
3. 修改`Studio 3T.vmoptions`文件, 添加`-javaagent`及jar包路径
```
示例: -javaagent:C:\Program Files\3T Software Labs\Studio 3T\studio_3t_crack-1.2.jar
```
## 3. 效果图
#### 效果图
![效果图](./image/result.png)
#### IDEA调试
![调试](./image/debug.png)
## 4. 备注
```bash
日志输出控制台: 3t.logToConsole
日志输出等级: 3t.logLevel
```

package com.crack;

/**
 * @author huangxd
 * @date 2020/3/12
 */
public enum VersionEnum {

	/**
	 * 版本枚举
	 */
	V2019_3_0("2019.3.0", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_4_0("2019.4.0", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_4_1("2019.4.1", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_5_0("2019.5.0", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_5_1("2019.5.1", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_6_0("2019.6.0", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_6_1("2019.6.1", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_7_0("2019.7.0", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2019_7_1("2019.7.1", "t3/common/lic/ag", "ac", "ag", "aS"),
	V2020_1_0("2020.1.0", "t3/common/lic/ag", "ac", "ag", "aS"),
	//部分方法名改变
	V2020_1_1("2020.1.1", "t3/common/lic/ag", "ad", "ah", "aS"),
	V2020_1_2("2020.1.2", "t3/common/lic/ag", "ad", "ah", "aS"),
	V2020_2_0("2020.2.0", "t3/common/lic/ag", "ad", "ah", "aS"),
	V2020_2_1("2020.2.1", "t3/common/lic/ag", "ad", "ah", "aS"),
	//类名,方法名改变
	V2020_3_0("2020.3.0", "t3/common/lic/ac", "aa", "ae", "aP"),
	V2020_3_1("2020.3.1", "t3/common/lic/ac", "aa", "ae", "aP");


	/**
	 * 软件版本
	 */
	private String version;
	/**
	 * 试用期类名
	 */
	private String className;
	/**
	 * 判断是否过期方法名
	 */
	private String expireStatusMethod;
	/**
	 * 剩余试用期天数方法名
	 */
	private String daysRemainingMethod;
	/**
	 * 试用期天数方法名
	 */
	private String probationaryMethod;

	VersionEnum(String version, String className, String expireStatusMethod, String daysRemainingMethod, String probationaryMethod) {
		this.version = version;
		this.className = className;
		this.expireStatusMethod = expireStatusMethod;
		this.daysRemainingMethod = daysRemainingMethod;
		this.probationaryMethod = probationaryMethod;
	}

	public String getVersion() {
		return version;
	}

	public String getClassName() {
		return className;
	}

	public String getExpireStatusMethod() {
		return expireStatusMethod;
	}

	public String getDaysRemainingMethod() {
		return daysRemainingMethod;
	}

	public String getProbationaryMethod() {
		return probationaryMethod;
	}
}
